//
//  MainViewController.m
//  Kevin BLE
//
//  Created by Kevin Lin on 13/9/26.
//  Copyright (c) 2013年 ? Corporation. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()<UITableViewDataSource, UITableViewDelegate>
@end

@implementation MainViewController
@synthesize TIBLEUISpinner;
@synthesize TIBLEUIConnBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"BLE";
    }
    return self;
}

/* Bluetooth notifications */
- (void)bluetoothAvailabilityChanged:(NSNotification *)notification {
    
    NSLog(@"NOTIFICATION:bluetoothAvailabilityChanged called. BT State: %d", [btManager enabled]);
    if ([btManager enabled]) {
    }
    else{
        [btManager setPowered:YES];
        [btManager setEnabled:YES];
    }
}


-(void)initKeyfog{
    [self.TIBLEUISpinner stopAnimating];
    [self.TIBLEUIConnBtn setEnabled:YES];
    t = [[TIBLECBKeyfob alloc] init];   // Init TIBLECBKeyfob class.
    [t controlSetup:1];                 // Do initial setup of TIBLECBKeyfob class.
    t.delegate = self;                  // Set TIBLECBKeyfob delegate class to point at methods implemented in this class.
    t.TIBLEConnectBtn = self.TIBLEUIConnBtn;
    [self performSelector:@selector(TIBLEUIScanForPeripheralsButton:) withObject:nil afterDelay:0.5];
}


-(void)WillResignActiveNotification{
//    [btManager setPowered:NO];
//    [btManager setEnabled:NO];
}

-(void)WillEnterForegroundNotification{
//    [btManager setPowered:YES];
//    [btManager setEnabled:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(bluetoothAvailabilityChanged:)
     name:@"BluetoothAvailabilityChangedNotification"
     object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(WillResignActiveNotification) name:UIApplicationWillResignActiveNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(WillEnterForegroundNotification) name:UIApplicationWillEnterForegroundNotification object:nil];

    [self.BLEUISoundBuzzerButton setEnabled:NO];
    btManager = [BluetoothManager sharedInstance];
    if (btManager.enabled) {
        [self performSelector:@selector(initKeyfog) withObject:nil];
    }
    else{
        [self.TIBLEUISpinner startAnimating];
        [self.TIBLEUIConnBtn setEnabled:NO];
        [self performSelector:@selector(initKeyfog) withObject:nil afterDelay:2.0];
    }
    
    self.TIBLEUIConnBtn.titleLabel.numberOfLines = 0;
}

- (IBAction)TIBLEUIScanForPeripheralsButton:(id)sender {
    if (t.activePeripheral) {
        if(t.activePeripheral.isConnected) {
            [[t CM] cancelPeripheralConnection:[t activePeripheral]];
            [self.BLEUISoundBuzzerButton setEnabled:NO];
            [TIBLEUIConnBtn setTitle:@"Scan and connect to KeyFob" forState:UIControlStateNormal];
            t.activePeripheral = nil;
        }
        else{
            NSLog(@"is Not Connected" );
        }
    } else {
        if (t.peripherals) t.peripherals = nil;
        [t findBLEPeripherals:5];
        //        [NSTimer scheduledTimerWithTimeInterval:(float)5.0 target:self selector:@selector(connectionTimer:) userInfo:nil repeats:NO];
        [TIBLEUISpinner startAnimating];
        [TIBLEUIConnBtn setTitle:@"Scanning.." forState:UIControlStateNormal];
    }
}

// Method from TIBLECBKeyfobDelegate, called when accelerometer values are updated
-(void) accelerometerValuesUpdated:(char)x y:(char)y z:(char)z {
    NSLog(@"%f , %f ,%f",(float)(x + 50) / 100, (float)(y + 50) / 100,(float)(z + 50) / 100);
//    TIBLEUIAccelXBar.progress = (float)(x + 50) / 100;
//    TIBLEUIAccelYBar.progress = (float)(y + 50) / 100;
//    TIBLEUIAccelZBar.progress = (float)(z + 50) / 100;
}

// Method from TIBLECBKeyfobDelegate, called when key values are updated
-(void) keyValuesUpdated:(char)sw {
    printf("Key values updated ! \r\n");
//    if (sw & 0x1) [TIBLEUILeftButton setOn:TRUE];
//    else [TIBLEUILeftButton setOn: FALSE];
//    if (sw & 0x2) [TIBLEUIRightButton setOn: TRUE];
//    else [TIBLEUIRightButton setOn: FALSE];
    
}
//Method from TIBLECBKeyfobDelegate, called when keyfob has been found and all services have been discovered
-(void) keyfobReady {
    [self.BLEUISoundBuzzerButton setEnabled:YES];
    [self.TIBLEUIConnBtn setEnabled:YES];
    [TIBLEUIConnBtn setTitle:@"Disconnect" forState:UIControlStateNormal];
    // Start battery indicator timer, calls batteryIndicatorTimer method every 2 seconds
//    [NSTimer scheduledTimerWithTimeInterval:(float)2.0 target:self selector:@selector(batteryIndicatorTimer:) userInfo:nil repeats:YES];
//    [t enableAccelerometer:[t activePeripheral]];   // Enable accelerometer (if found)
//    [t enableButtons:[t activePeripheral]];         // Enable button service (if found)
    [t enableTXPower:[t activePeripheral]];         // Enable TX power service (if found)
    [t enableLinkLoss:[t activePeripheral]];
    [TIBLEUISpinner stopAnimating];
}

-(void) TXPwrLevelUpdated:(char)TXPwr {
}

-(void)peripheralDidUpdateRSSI:(CBPeripheral *)p RSSI:(NSNumber *)RSSI{
    self.RSSILabel.text = [NSString stringWithFormat:@"%d",[RSSI intValue]];
    
    self.estimatedDistanceLabel.textColor = [UIColor blueColor];
    self.RSSILabel.textColor = [UIColor blueColor];
    if ([RSSI integerValue] > -70) {
        self.estimatedDistanceLabel.text = @"0.5m";
    }
    else if([RSSI integerValue] <= -70 && [RSSI integerValue] > -80){
        self.estimatedDistanceLabel.text = @"2m";
    }
    else if ([RSSI integerValue] <= -80 && [RSSI integerValue] > -85)
    {
        self.estimatedDistanceLabel.text = @"5m";
    }
    else if ([RSSI integerValue] <= -85 && [RSSI integerValue] > -90)
    {
        self.estimatedDistanceLabel.text = @"7m";
    }
    else if ([RSSI integerValue] <= -90 && [RSSI integerValue] > -100)
    {
        self.estimatedDistanceLabel.text = @"10m";
    }
    else if ([RSSI integerValue] <= -100)
    {
        self.estimatedDistanceLabel.text = @"Over 10m";
    }
    
    if ([RSSI integerValue] <= -105){
        self.estimatedDistanceLabel.textColor = [UIColor redColor];
        self.RSSILabel.textColor = [UIColor redColor];
    }

    
}

-(void)startScanningForPeripherals{
    if (!self.peripherals) {
        self.peripherals = [NSMutableArray array];
    }
    else{
        [self.peripherals removeAllObjects];
    }
    [self.TIBLETableView reloadData];
}


-(void)didDiscoverPeripheral:(CBPeripheral *)p{
    if ([self.peripherals count]!=0) {
        for (CBPeripheral *peripheral in self.peripherals) {
            if ([peripheral.name isEqualToString:p.name]) {
                continue;
            }
            
            [self.peripherals addObject:p];
        }
    }
    else{
        [self.peripherals addObject:p];
    }
    [self.TIBLETableView reloadData];
    [self.TIBLEUIConnBtn setEnabled:NO];
}

-(void)didDisconnectPeripheral:(CBPeripheral *)p
{
    UIAlertView *alert =[[ UIAlertView alloc]initWithTitle:NSLocalizedString(@"Loss Connection", nil) message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
    [alert show];
    if (t.peripherals) t.peripherals = nil;
    [t findBLEPeripherals:5];
    //       [NSTimer scheduledTimerWithTimeInterval:(float)5.0 target:self selector:@selector(connectionTimer:) userInfo:nil repeats:NO];
    
    [TIBLEUISpinner startAnimating];
    [TIBLEUIConnBtn setTitle:@"Scanning.." forState:UIControlStateNormal];
}

// Called when scan period is over to connect to the first found peripheral
-(void) connectionTimer:(NSTimer *)timer {
    /*    if(t.peripherals.count > 0)
     {
     for (CBPeripheral *p in t.peripherals) {
     if ([p.name rangeOfString:@"Keyfob"].location != NSNotFound) {
     [t connectPeripheral:p];
     printf("Equal\n");
     } else {
     printf("Not equal\n");
     }
     }
     //[t connectPeripheral:[t.peripherals objectAtIndex:0]];
     
     }
     else
     */
    [TIBLEUISpinner stopAnimating];
    [TIBLEUIConnBtn setTitle:@"Scan and connect to KeyFob" forState:UIControlStateNormal];
}

- (IBAction)TIBLEUISoundBuzzerButton:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"BEEP"]) {
        [button setTitle:@"STOP" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
          [t soundBuzzer:0x02 p:[t activePeripheral]]; //Sound buzzer with 0x02 as data value
    }
    else{
        [button setTitle:@"BEEP" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [t soundBuzzer:0x00 p:[t activePeripheral]]; //Sound buzzer with 0x02 as data value
    }
}

- (IBAction)TIBLEUISearchButton:(id)sender{
    [self.TIBLEUIConnBtn setEnabled:YES];
    [self TIBLEUIScanForPeripheralsButton:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.peripherals count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    if ([self.peripherals count]!=0) {
        CBPeripheral *peripheral = ((CBPeripheral*)[self.peripherals objectAtIndex:indexPath.row]);
        cell.textLabel.text = peripheral.name;
        cell.detailTextLabel.numberOfLines= 2;
        cell.detailTextLabel.font = [UIFont systemFontOfSize:10];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"UUID:\n%s", [t UUIDToString:peripheral.UUID]];
    }

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.peripherals count]!=0) {
        if (t.activePeripheral) {
            if (t.activePeripheral.isConnected) {
                [[t CM] cancelPeripheralConnection:[t activePeripheral]];
            }
        }
        CBPeripheral *peripheral = ((CBPeripheral*)[self.peripherals objectAtIndex:indexPath.row]);
        [t connectPeripheral:peripheral];
        self.DeviceNameLabel.text = peripheral.name;
    }
}

@end
