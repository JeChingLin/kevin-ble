//
//  AppDelegate.h
//  Kevin BLE
//
//  Created by Kevin Lin on 13/9/26.
//  Copyright (c) 2013年 ? Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MainViewController *viewController;
@end
