//
//  main.m
//  Kevin BLE
//
//  Created by Kevin Lin on 13/9/26.
//  Copyright (c) 2013年 ? Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
