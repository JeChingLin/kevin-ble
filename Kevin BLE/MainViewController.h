//
//  MainViewController.h
//  Kevin BLE
//
//  Created by Kevin Lin on 13/9/26.
//  Copyright (c) 2013年 ? Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TIBLECBKeyfob.h"
#import "BluetoothManager.h"

@interface MainViewController : UIViewController <TIBLECBKeyfobDelegate,UIAlertViewDelegate>
{
    TIBLECBKeyfob *t; //TI keyfob class (private)
    int count;
    
    // bluetooth manager
    BluetoothManager *btManager;
    UIBackgroundTaskIdentifier backgroundUpdateTask;
}
@property (weak, nonatomic) IBOutlet UILabel *DeviceNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *RSSILabel;
@property (weak, nonatomic) IBOutlet UILabel *TxPowerLabel;
@property (weak, nonatomic) IBOutlet UILabel *estimatedDistanceLabel;
@property (weak, nonatomic) IBOutlet UIButton *TIBLESearchBtm;
@property (weak, nonatomic) IBOutlet UIButton *BLEUISoundBuzzerButton;
@property (weak, nonatomic) IBOutlet UIButton *TIBLEUIConnBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *TIBLEUISpinner;
@property(nonatomic, strong)IBOutlet UITableView *TIBLETableView;
@property(nonatomic, strong) NSMutableArray *peripherals;
- (IBAction)TIBLEUISearchButton:(id)sender;
- (IBAction)TIBLEUIScanForPeripheralsButton:(id)sender;
- (IBAction)TIBLEUISoundBuzzerButton:(id)sender;

@end
